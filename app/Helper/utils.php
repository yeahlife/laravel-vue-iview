<?php
if(! function_exists('laravelVersion')) {
    function laravelVersion()
    {
        $app = app();
        return $app::VERSION;
    }
}

if(!function_exists('json')) {
    function json($status, $message = '',$data = [])
    {
        $request = request();
        $requestID = generateNonceStr(12);
        $return = [];
        $return['request_id'] = $requestID;
        $return['result_code'] = $status;
        !empty($message) && $return['message'] = $message;
        !empty($data) && $return['data'] = $data;

        // 记录日志
        saveLog([
            'request_id' => $requestID,
            'method' => $request->method(),
            'fullUrl' => $request->fullUrl(),
            'header' => $request->header(),
            'input_data' => $request->input(),
            'ips' => $request->ips(),
            'response' => $return,
            'request_time' => $request->server('REQUEST_TIME'),
            'response_time' => date('Y-m-d H:i:s'),
        ]);

        //返回数据
        return response()->json($return);
    }
}

if(!function_exists('httpGet')) {
    function httpGet($url, $headers = []) 
    {
        $client = new \GuzzleHttp\Client();
        $requestTime = time();
        $response = $client->get($url, $headers);

        if($response->getStatusCode() != 200) {
            // 异常处理
            saveErrorLog([
                'method' => 'GET',
                'fullUrl' => $url,
                'header' => $headers,
                'response' => $response,
                'request_time' => date('Y-m-d H:i:s', $request_time),
                'response_time' => date('Y-m-d H:i:s'),
            ], 'api');

            return false;
        }

        return $response->getBody()->getContents();
    }
}

if(!function_exists('httpGetAsync')) {
    function httpGetAsync($url, $headers = []) 
    {
        $client = new \GuzzleHttp\Client();
        return $client->getAsync($url, $headers);
    }
}

if(!function_exists('httpPost')) {
    function httpPost($url, $data = [], $headers = [], $type = 'formdata') {
        $requestTime = time();
        $client = new \GuzzleHttp\Client();
    
        $options = [];
        if($type == 'json') {
            $headers['content-type']  = 'application/json';
            $options['header'] = $headers;
            $options['body'] = json_encode($data, JSON_UNESCAPED_UNICODE);
        } elseif ($type == 'formdata') {
            $options['form_params'] = $data;
        } else {
            $options['body'] = $data;
        }

        $response = $client->post($url, $options);

        if($response->getStatusCode() != 200) {
            // 异常处理
            saveErrorLog([
                'method' => 'POST',
                'fullUrl' => $url,
                'header' => $headers,
                'input_data' => $data,
                'response' => $response,
                'request_time' => date('Y-m-d H:i:s', $request_time),
                'response_time' => date('Y-m-d H:i:s'),
            ], 'api');
            return false;
        }

        return $response->getBody()->getContents();
    }
}

if(!function_exists('httpPostByJson')) {
    function httpPostByJson($url, $data = [], $headers = []) {
        return httpPost($url, $data, $headers, 'json');
    }
}

if(!function_exists('httpPostAsync')) {
    function httpPostAsync($url, $data = [], $headers = [], $type = 'formdata') {
        $client = new \GuzzleHttp\Client();
    
        $options = [];
        if($type == 'json') {
            $headers['content-type']  = 'application/json';
            $options['header'] = $headers;
            $options['body'] = json_encode($data, JSON_UNESCAPED_UNICODE);
        } elseif ($type == 'formdata') {
            $options['form_params'] = $data;
        } else {
            $options['body'] = $data;
        }

        return $client->postAsync($url, $options);
    }
}

if(!function_exists('generateNonceStr')) {
    function generateNonceStr($len = 16)
    {
        $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $token = "";
        for ($i = 0; $i < $len; ++$i) {
            $token .= $chars[mt_rand(1, 62) - 1];
        }
        return $token;
    }
}

if(!function_exists('saveLog'))
{
    function saveLog($data, $logger = 'laravel', $level = 'info', $file = 'laravel') 
    {
        $log = new \Monolog\Logger($logger);
        $logPath = storage_path() . '/logs/' . $file . '-' . date('Y-m-d', time()) . '.log';
        switch($level) {
            case 'info':
                $logLevel =  \Monolog\Logger::INFO;
                break;
            case 'error':
                $logLevel =  \Monolog\Logger::ERROR;
                break;
            case 'notice':
                $logLevel =  \Monolog\Logger::NOTICE;
                break;
            case 'warning':
                $logLevel =  \Monolog\Logger::WARNING;
                break;
            default:
                $logLevel =  \Monolog\Logger::INFO;
                break;
        }
        $log->pushHandler(new \Monolog\Handler\StreamHandler($logPath, $logLevel));
        $log->$level( json_encode($data, JSON_UNESCAPED_UNICODE) );
    }
}

if(!function_exists('saveErrorLog'))
{
    function saveErrorLog($data, $logger) 
    {
        saveLog($data, $logger, 'error', 'error');
    }
}

if(!function_exists('upload'))
{
    function upload($file, $type = 'image')
    {
        if(!$file->isValid()){  
            return false;
        }

        //扩展名  
        $ext = $file->getClientOriginalExtension();
        //文件类型  
        $type = $file->getClientMimeType();
        //临时绝对路径  
        $realPath = $file->getRealPath();

        $filename = date('Ym') . DIRECTORY_SEPARATOR . uniqid() . '.' . $ext;

        $bool = Storage::disk('public')->put($filename, file_get_contents($realPath));
        
        if(!$bool) {
            return false;
        }

        return Storage::url($filename);
    }
}

if(!function_exists('user')) 
{
    function user()
    {
        return Auth::guard()->user();
    }
}