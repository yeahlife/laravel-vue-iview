<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class UserController extends Controller
{
    use AuthenticatesUsers;
    public function __construct()
    {
        $this->middleware('auth')->except('login');
    }

    public function username()
    {
        return 'name';
    }
    
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);
            $user = $this->guard()->user();
            return json('success', '登录成功.', $user);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return json('error', '用户名或密码不正确.');
    }


    public function logout(Request $request)
    {
        $this->guard()->logout();
        return json('success', '退出成功.');
    }

    public function resetPassword(Request $request)
    {
        $input = $request->input();
        $user = $this->guard()->user();
        $credentials = [
            'name' => $user->name,
            'password' => $input['oldPassword']
        ];

        if(!$this->guard()->attempt($credentials)) {
            return json('error', '密码验证失败');
        }

        $user->password = bcrypt($input['password']);

        if($user->save()) {
            return json('success', '密码修改成功');
        }

        return json('error', '修改密码失败，请稍后重试');
    }

    public function profile(Request $request)
    {
        $user = $this->guard()->user();
        $user->profile;

        return json('success', '获取用户资料成功' ,$user);
    }

    public function unlock(Request $request)
    {
        $user = $this->guard()->user();
        $credentials = [
            'name' => $user->name,
            'password' => $request->input('password')
        ];
        if($this->guard()->attempt($credentials)) {
            return json('success', '密码正确');
        }

        return json('error', '验证失败');
    }

    public function screenLock(Request $request)
    {
        
    }
}
