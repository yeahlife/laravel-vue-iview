<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            $model->saveContent();
        });
    }

    public function detail()
    {
        return $this->hasOne('App\TopicDetail');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'author');
    }

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category');
    }


    public function saveContent()
    {
        $request = request();
        if($request->has('content')) {
            $content = $request->input('content');
            $topicDetail = TopicDetail::where('topic_id', $this->id)->first();
            if(!$topicDetail) {
                $topicDetail = new TopicDetail();
                $topicDetail->topic_id = $this->id;
            } 

            $topicDetail->content = $request->input('content');
            $topicDetail->save();
        }
    }
}
