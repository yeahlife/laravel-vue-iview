<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicDetail extends Model
{
    protected $table = 'topic_detail';
    protected $primaryKey = 'topic_id';
    public $timestamps = false;
}
