<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 200)->nullable(false);
            $table->integer('author')->nullable(false);
            $table->integer('category')->nullable(false);
            $table->string('cover')->nullable();
            $table->string('keywords')->nullable();
            $table->string('description')->nullable();
            $table->integer('view_count')->default(1);
            $table->integer('reply_count')->default(0);
            $table->integer('publish_time')->nullable();
            $table->smallInteger('status')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}
