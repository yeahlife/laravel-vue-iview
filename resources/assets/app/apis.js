const apis = {
    login: '/api/login',
    logout: '/api/logout',
    profile: '/api/profile',
    unlock: '/api/unlock',
    resetPassword: '/api/reset_password'
};

export default apis;