import axios from 'axios';
import store from '../store';
import {Message} from 'iview';

const service = axios.create({
    validateStatus: function (status) {
        return status <= 500
    }
});

service.interceptors.request.use(config => {
    return config;
});

service.interceptors.response.use(response => {
    if (response.status == 401) {
        store.commit('logout');
        return;
    }

    return response.data;
});

export default service;