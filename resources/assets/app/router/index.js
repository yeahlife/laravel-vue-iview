import Vue from 'vue';
import VueRouter from 'vue-router';
import Cookie from 'js-cookie';
import {routers, loinRouter, lockingRouter} from './router';
import iView from 'iview';
import store from '../store';
Vue.use(VueRouter);

// 路由配置
const RouterConfig = {
    mode: 'history',
    routes: routers
};

export const router = new VueRouter(RouterConfig);

// 前置操作
router.beforeEach((to, from, next) => {
    iView.LoadingBar.start();
    if(!Cookie.get('login') && to.name !== 'login') {
        next({name: 'login'});
    } else if (Cookie.get('login') && to.name === 'login'){
        next({name: 'index'});
    } else {
        store.commit('refresh');
        next();
    }
});

// 后置操作
router.afterEach((to, from) => {
    iView.LoadingBar.finish();
});