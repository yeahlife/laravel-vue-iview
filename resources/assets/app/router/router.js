import Main from '../template/main.vue';

export const loginRouter = {
    path: '/cms/login',
    name: 'login',
    component: () => import('../template/login.vue')
};

export const lockingRouter = {
    path: '/cms/locking',
    name: 'locking',
    component: () =>
        import ('../components/lockscreen/components/locking-page.vue')
};

export const appRouter = {
    path: '/cms',
    name: 'index',
    redirect: '/cms/dashboard',
    component: Main,
    children: [
        {path: '/cms/dashboard', title: '首页', name: 'home_index', component: () => import('../template/home/dashboard.vue')},
        {path: '/cms/message', title: '消息中心', name: 'message_index', component: () => import('../template/message/index.vue')},
        {path: '/cms/user', title: '个人中心', name: 'user_index', component: () => import('../template/user/index.vue')}
    ]
};

export const routers = [
    loginRouter,
    lockingRouter,
    appRouter
];