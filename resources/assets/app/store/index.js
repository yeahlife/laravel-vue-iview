import Vue from 'vue';
import Vuex from 'vuex';
import Cookies from 'js-cookie';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {

    },
    getters: {
        // user: () => JSON.parse(Cookies.get('user'))
    },
    mutations: {
        logout (state) {
            Cookies.remove('user');
            Cookies.remove('login');
            state.login = false;
            state.user = false;
        },
        login (state, user) {
            Cookies.set('user', user);
            Cookies.set('login', true);
            state.login = true;
            state.user = user;
        },
        refresh (state) {
            let user = Cookies.get('user');
            let login = Cookies.get('login');
            if(!login || !user) {
                this.commit('logout');
                return;
            }

            state.user = JSON.parse(user);
            state.login = login == 'true' ? true : false;

        }
    },
    actions: {
        //
    },
    modules : {
        //
    }
});

export default store;
