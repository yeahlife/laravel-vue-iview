<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

// 后台路由
Route::view('/cms', 'cms');
Route::view('/cms/{query}', 'cms')->where('query', '.*');

Route::group(['prefix' => 'api'], function () {
    Route::post('login', 'UserController@login');
    Route::post('logout', 'UserController@logout');
    Route::post('reset_password', 'UserController@resetPassword');
    Route::post('unlock', 'UserController@unlock');
    Route::get('profile', 'UserController@profile');
});